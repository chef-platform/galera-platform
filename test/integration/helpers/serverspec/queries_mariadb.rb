# frozen_string_literal: true

#
# Copyright (c) 2015-2016 Sam4Mobile, 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

hosts = [
  'galera-platform-kitchen-1',
  'galera-platform-kitchen-2',
  'galera-platform-kitchen-3'
]

dbname = host_inventory['hostname'].tr('-', '_')

def mysql_query(host, query)
  `mysql -h #{host} -u root -prandompass -Ne "#{query}"`
end

createdb_cmd = "create database if not exists test_#{dbname}"
checkdb_cmd = "show databases like 'test_#{dbname}'"

mysql_query('localhost', createdb_cmd)

describe 'Replication' do
  hosts.each do |server|
    it "#{server} should have serverspec_#{dbname} database created" do
      expect(mysql_query(server, checkdb_cmd)).to include("test_#{dbname}")
    end
  end
end
