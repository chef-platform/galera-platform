# frozen_string_literal: true

#
# Copyright (c) 2016 Sam4Mobile, 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Use ClusterSearch
::Chef::Recipe.send(:include, ClusterSearch)

# Looking for members of cluster
cluster = cluster_search(node[cookbook_name])
return if cluster.nil? # return (wait for next run) if we found nothing

# Check if initiator_id is a correct value
initiator_id = node[cookbook_name]['initiator_id']
if initiator_id < 1 || initiator_id > cluster['hosts'].size
  raise 'Invalid initiator_id, should be between 1 and cluster.size'
end

raise 'Cannot find myself in the cluster' if cluster['my_id'] == -1

# Main configuration
config = node[cookbook_name]['config'].to_hash
galera_conf = config['galera']
server_cnf = config['server.cnf']

# Load credentials from (encrypted) data bag
db_attr = node[cookbook_name]['data_bag']
db_item = begin data_bag_item(db_attr['name'], db_attr['item'])
          rescue StandardError
            {}
          end

# Password for root, prefered way is via an encrypted data bag but a
# fallback is possible via an attribute
root_pw = db_item[db_attr['key']] || node[cookbook_name]['root_password']

# Get default sst username and password
user_attr, pwd_attr = galera_conf['wsrep_sst_auth'].split(':')

sst_user = db_item[db_attr['sst_user']] || user_attr
sst_password = db_item[db_attr['sst_password']] || pwd_attr
sst_password = root_pw if sst_user == 'root'

# Set authentification configuration
galera_conf['wsrep_sst_auth'] = "#{sst_user}:#{sst_password}"

# Build the cluster address
galera_conf['wsrep_cluster_address'] = "gcomm://#{cluster['hosts'].join(',')}"
galera_conf['wsrep_node_address'] = node['fqdn']

# Deploy config
ini_conf = systemd_unit 'galera config.to_ini' do
  content config
  action :nothing
end.to_ini

file '/etc/my.cnf.d/server.cnf' do
  content ini_conf
  owner server_cnf['owner']
  group server_cnf['group']
  mode server_cnf['mode']
  sensitive true
end

# Set the root password (auto-login)
file '/root/.my.cnf' do
  content "# Produced by Chef\n[client]\nuser = root\npassword = #{root_pw}\n"
  mode '0640'
  owner 'root'
  group 'root'
  only_if { node[cookbook_name]['root_autologin'] }
  sensitive true
end

# Bootstrap has to be executed only on the first node of cluster
if cluster['my_id'] == initiator_id
  execute 'cluster-bootstrap' do
    command <<-BASH # the sed is to fix a bug in 10.3.x
      sed -i 's/return $extcode/exit $extcode/g' $(which galera_new_cluster) \
      && galera_new_cluster && \
      touch /etc/my.cnf.d/cluster_bootstraped
    BASH
    creates '/etc/my.cnf.d/cluster_bootstraped'
  end

  # Secure installation
  # For commodity we allow root user to connect from any host
  create_sst_user = ''
  unless sst_user == 'root'
    sst_user_privileges = node[cookbook_name]['sst_user_privileges']
    create_sst_user = <<-SQL
      CREATE USER '#{sst_user}'@'%' IDENTIFIED BY '#{sst_password}';\
      GRANT #{sst_user_privileges} ON *.* TO '#{sst_user}'@'%';
    SQL
  end

  execute 'secure galera installation' do
    command <<-SQL
      mysql -u root -p'' -e "\
      START TRANSACTION;
      UPDATE mysql.user SET Password=PASSWORD('#{root_pw}') WHERE User='root';\
      DELETE FROM mysql.user WHERE User='';\
      #{create_sst_user}\
      DROP DATABASE test;\
      DELETE FROM mysql.db WHERE Db='test' OR Db='test\_%';\
      UPDATE mysql.user SET Host='%' WHERE Host='#{node['fqdn']}';\
      FLUSH PRIVILEGES;\
      COMMIT;" \
      && touch /etc/my.cnf.d/installation_secured
    SQL
    creates '/etc/my.cnf.d/installation_secured'
    sensitive true
  end
else
  # Check if initiator has bootstraped and create a file in this case
  retry_number = node[cookbook_name]['bootstrap_check_retry_number']
  execute 'check bootstrap status on initiator' do
    command <<-SQL
    for (( try=1; try<=#{retry_number}; try++ )); do
      mysql --connect_timeout 2 \
      -h #{cluster['hosts'][initiator_id - 1]} -u root -p#{root_pw} \
      -e "use mysql;" \ >/dev/null 2>&1 \
      && touch /etc/my.cnf.d/cluster_bootstraped
      if [ -e /etc/my.cnf.d/cluster_bootstraped ]; then
        break
      fi
      sleep 5
    done
    true
    SQL
    creates '/etc/my.cnf.d/cluster_bootstraped'
    ignore_failure true
  end
end
