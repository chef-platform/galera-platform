Changelog
=========

3.2.0
-----

Main:

- feat: make server.cnf file resource configurable

Tests:

- use chefplatform image
- fix: add rspec-core to Gemfile
- fix: accept chef license
- rename kitchen config file

Misc:

- style(rubocop): fix use e instead of error
- style(rubocop): add FrozenStringLiteralComment

3.1.0
-----

Main:

- feat: fix and use 10.3 by default
- feat: allow specific user for sst synchronization
- feat: allow the use of attribute for root password

Misc:

- chore: fix issues url in metadata
- chore: add supermarket category in .category
- chore: set generic maintainer & helpdesk email

3.0.0
-----

Main:

- feat: allow generic config, remove percona support
  + allow other configuration sections than 'galera' to be configurable
  +  move 'datadir' in 'mysqld' to fix sync/startup in latest version
  +  replace xtrabackup by mariabackup (which is better supported by
     mariadb) and remove percona support
- chore: handover maintenance to make.org
- fix: do not set retries if package\_retries is nil (Chef 13)
- refactor: remove templates

Tests:

- test: run tests on all nodes
- test: use kitchen\_command.rb from test-cookbook
- test: include .gitlab-ci.yml from test-cookbook
- test: move driver config, set build\_pull and replace require\_chef\_omnibus

Misc:

- chore: set new contributing guide with karma style
- chore: add foodcritic & rubocop in Gemfile
- style: use cookbook\_name macro everywhere
- style(rubocop): fix heredoc & misc offenses
- doc: update README on new sync mode

2.0.0
-----

Main:

- Support mariadb 10.2 and do not support older version
- Add auto-restart option: restart if config changes (false by default)

Test:

- Set docker seccomp policy to undefined
- Use template 20160914 of .gitlab-ci.yml

1.0.0
-----

- Initial version for RHEL Family 7 (tested on Centos 7) supporting MariaDB by
  default (should be able to support MySQL too).
