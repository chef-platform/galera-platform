# frozen_string_literal: true

name 'galera-platform'
maintainer 'Chef Platform'
maintainer_email ' incoming+chef-platform/galera-platform@incoming.gitlab.com'
license 'Apache-2.0'
description 'Install and configure a Galera Cluster'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/chef-platform/galera-platform'
issues_url 'https://gitlab.com/chef-platform/galera-platform/issues'
version '3.3.0'

supports 'centos', '>= 7.1'

chef_version '>= 13.0'

depends 'cluster-search'
