# frozen_string_literal: true

#
# Copyright (c) 2016 Sam4Mobile, 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Define useful cookbook_name macro
cookbook_name = 'galera-platform'

# Configuration about the encrypted data bag containing the keys
default[cookbook_name]['data_bag']['name'] = 'secrets'
default[cookbook_name]['data_bag']['item'] = 'mariadb-credentials'
# Keys used to load the value in data bag containing passwords and usernames
default[cookbook_name]['data_bag']['key'] = 'password'
default[cookbook_name]['data_bag']['sst_user'] = 'sst_user'
default[cookbook_name]['data_bag']['sst_password'] = 'sst_password'

# Password for root: prefered way is via an encrypted data bag but a
# fallback is possible via this attribute
default[cookbook_name]['root_password'] = 'unsafe_password'

# Cluster configuration with cluster-search
# Role used by the search to find other nodes of the cluster
default[cookbook_name]['role'] = cookbook_name
# Hosts of the cluster, deactivate search if not empty
default[cookbook_name]['hosts'] = []
# Expected size of the cluster. Ignored if hosts is not empty
default[cookbook_name]['size'] = 3

# Initiator id define the node that will perform cluster creation
default[cookbook_name]['initiator_id'] = 1

# Default yum repository url for DB backend
default[cookbook_name]['repo_url'] =
  'http://yum.mariadb.org/10.3/centos7-amd64/'
# GPG key associated to DB backend repository
default[cookbook_name]['repo_gpgkey'] =
  'https://yum.mariadb.org/RPM-GPG-KEY-MariaDB'

# Packages to install
# contains some util packages like which and socat and MariaDB packages
default[cookbook_name]['packages'] = [
  'which', 'socat', 'MariaDB-server', 'MariaDB-backup'
]

# MariaDB global cluster node configuration
# https://mariadb.com/kb/en/library/getting-started-with-mariadb-galera-cluster
default[cookbook_name]['config'] = {
  'mysqld' => {
    'datadir' => '/var/lib/mysql'
  },
  'galera' => {
    'wsrep_on' => 'ON',
    'wsrep_provider' => '/usr/lib64/galera/libgalera_smm.so',
    'binlog_format' => 'row',
    'default_storage_engine' => 'InnoDB',
    'innodb_autoinc_lock_mode' => '2',
    'bind-address' => '0.0.0.0',
    'wsrep_cluster_name' => 'MariaDB_Cluster',
    # SST is the method used by the cluster to provisions nodes by transferring
    # a full data copy from one node to another.
    # mariabackup method is the preferred method as it is non-blocking (it does
    # not require to put donor in read-only). rsync method is also possible.
    'wsrep_sst_method' => 'mariabackup',
    # Should be specified in data bag (more secure) but can be defined here
    # PASSWORD is taken into account only if user != root
    'wsrep_sst_auth' => 'root:PASSWORD'
  },
  'server.cnf' => {
    # path is not configurable yet because it is hardcoded elsewhere
    # 'path' => '/etc/my.cnf.d',
    'owner' => 'root',
    'group' => 'mysql',
    'mode' => '0640'
  }
}

# Set the privileges for the user created to perform sst synchronization
default[cookbook_name]['sst_user_privileges'] =
  'PROCESS, RELOAD, LOCK TABLES, REPLICATION CLIENT'

# Deploy password for root user to allow easy db login
default[cookbook_name]['root_autologin'] = true

# Auto restart when configuration files change
default[cookbook_name]['auto_restart'] = false

# Configure retries for galera backend service
# retry if no donors are available to provide the initial dump through the
# configured sst_method
default[cookbook_name]['service']['retries_number'] = nil
default[cookbook_name]['service']['retry_delay'] = nil

# Numbers of connection attempts to check if initiator has bootstrapped
default[cookbook_name]['bootstrap_check_retry_number'] = 1

# Configure retries for the package resources, default = global default (0)
# (mostly used for test purpose)
default[cookbook_name]['package_retries'] = nil
